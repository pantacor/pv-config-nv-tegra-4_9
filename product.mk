TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := aarch64
TARGET_UBOOT_ARCH := arm

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/aarch64-linux-musl-cross/bin/aarch64-linux-musl-
TARGET_LINUX_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/aarch64-linux-musl-cross/bin/aarch64-linux-musl-

TARGET_GLOBAL_CFLAGS += -march=armv8-a

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TARGET_VENDOR_DIR)/skel

#TARGET_LINUX_DEVICE_TREE := amlogic/meson64_odroidc2.dtb
TARGET_LINUX_DEVICE_TREE := tegra210-jetson-tx1-p2597-2180-a01-devkit.dtb

LINUX_CONFIG_TARGET := tegra_defconfig
TARGET_LINUX_CONFIG_MERGE_FILES := yes

